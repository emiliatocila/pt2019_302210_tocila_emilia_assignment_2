package PT2019.H2.H2Project;

public class Main {

	public static void main(String[] args) {
		View view = new View();
        Controller controller = new Controller(view);
        view.setVisible(true);
	}
}
